/***************************************************************************//**
 * @file
 * @brief LEUART/DMA in EM2 example for EFM32ZG_STK3200 starter kit
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_leuart.h"
#include "em_dma.h"
#include "dmactrl.h"

#include "csc.h"
#include "delay_func.h"


/* HAL */
#include "leuart_func.h"

#if 0
/** LEUART Rx/Tx Port/Pin Location */
#define LEUART_LOCATION    0
#define LEUART_TXPORT      gpioPortD        /* LEUART transmission port */
#define LEUART_TXPIN       4                /* LEUART transmission pin  */
#define LEUART_RXPORT      gpioPortD        /* LEUART reception port    */
#define LEUART_RXPIN       5                /* LEUART reception pin     */

/** DMA Configurations            */
#define DMA_CHANNEL       0          /* DMA channel is 0 */
#define NUM_TRANSFER      100       /* Number of transfers per DMA cycle */
#define SUCCESS 			0

/** DMA callback structure */
static DMA_CB_TypeDef dmaCallBack;
#endif

#define BUFFERSIZE		100
uint8_t leuart_rx_buffer[BUFFERSIZE] = "";
char answer[12] = "mac_tx_ok\r\n";
uint8_t rn2903_cmd[] = "mac tx uncnf 1 3031323334\r\n";

volatile int rx_flag = 0;
#if 0
/***************************************************************************//**
 * @brief  DMA callback function
 *
 * @details This function is invoked once a DMA transfer cycle is completed.
 *          It then refreshes the completed DMA descriptor.
 ******************************************************************************/
static void basicTransferComplete(unsigned int channel, bool primary, void *user)
{
  (void) user; /* Unused parameter */
  /* Refresh DMA basic transaction cycle */
  DMA_ActivateBasic(channel,
                    primary,
                    false,
                    (void *)&leuart_rx_buffer,
                    (void *)&LEUART0->RXDATA,
                    (NUM_TRANSFER - 1));
}

/***************************************************************************//**
 * @brief  Setup DMA
 *
 * @details
 *   This function initializes DMA controller.
 *   It configures the DMA channel to be used for LEUART0 transmit and receive.
 *   The primary descriptor for channel0 is configured for N_MINUS_1 byte
 *   transfer. For continous data reception and transmission using LEUART in
 *   basic mode DMA Callback function is configured to reactivate
 *   basic transfer cycle once it is completed.
 *
 ******************************************************************************/
void setupDma(void)
{
  /* DMA configuration structs */
  DMA_Init_TypeDef       dmaInit;
  DMA_CfgChannel_TypeDef channelCfg;
  DMA_CfgDescr_TypeDef   descrCfg;

  /* Initializing the DMA */
  dmaInit.hprot        = 0;
  dmaInit.controlBlock = dmaControlBlock;
  DMA_Init(&dmaInit);

  /* Set the interrupt callback routine */
  dmaCallBack.cbFunc = basicTransferComplete;
  /* Callback doesn't need userpointer */
  dmaCallBack.userPtr = NULL;

  /* Setting up channel */
  channelCfg.highPri   = false; /* Can't use with peripherals */
  channelCfg.enableInt = true;  /* Enabling interrupt to refresh DMA cycle*/

  /* Configure channel 0 */
  /*Setting up DMA transfer trigger request*/
  channelCfg.select = DMAREQ_LEUART0_RXDATAV;
  /* Setting up callback function to refresh descriptors*/
  channelCfg.cb     = NULL;
  DMA_CfgChannel(0, &channelCfg);

  /* Setting up channel descriptor */
  /* Destination is LEUART_Tx register and doesn't move */
  descrCfg.dstInc = dmaDataInc1 ;

  /* Source is LEUART_RX register and transfers 8 bits each time */
  descrCfg.srcInc = dmaDataIncNone;
  descrCfg.size   = dmaDataSize1;

  /* Default setting of DMA arbitration*/
  descrCfg.arbRate = dmaArbitrate1;
  descrCfg.hprot   = 0;

  /* Configure primary descriptor */
  DMA_CfgDescr(DMA_CHANNEL, true, &descrCfg);

}
#endif
/***************************************************************************//**
 * @brief  Main function
 ******************************************************************************/
int main(void)
{
  /* Chip errata */
    CHIP_Init();

  /* Initialize LEUART */
    leuart_setup();

  /* Setup DMA */
    leuart_dma_setup();
#if 0
    uint8_t str_sz = 50;
    uint8_t sub_str_sz = 11;
#endif



    for(;;)
    {
        LEUART_Enable(LEUART0, leuartEnable);
        strcat((char *)&leuart_rx_buffer[0], (const char *)"hola\r\n");
        leuart_string_tx(rn2903_cmd);
        leuart_dma_rx_timeout(leuart_rx_buffer,7000);
        leuart_string_tx(leuart_rx_buffer);
        delay_c(1000);
        memset(leuart_rx_buffer, '\0', BUFFERSIZE);
        LEUART_Enable(LEUART0, leuartDisable);

    }
}
